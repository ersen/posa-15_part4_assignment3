package com.example.weatherserviceapp.jsonweather;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.util.JsonReader;
import android.util.JsonToken;

/**
 * Parses the Json weather data returned from the Weather Services API
 * and returns a List of JsonWeather objects that contain this data.
 */
public class WeatherJSONParser2 {
	/**
	 * Used for logging purposes.
	 */
	private final String TAG =
			this.getClass().getCanonicalName();

	/**
	 * Parse the @a inputStream and convert it into a List of JsonWeather
	 * objects.
	 */
	public List<JsonWeather> parseJsonStream(InputStream inputStream)
			throws IOException {

		// Create a JsonReader for the inputStream.
		try (JsonReader reader =
				new JsonReader(new InputStreamReader(inputStream, "UTF-8"))) {

			return parseJsonWeatherArray(reader);
		}

	}

	/**
	 * Parse a single Json stream and convert it into a JsonWeather
	 * object.
	 */
	public JsonWeather parseJsonStreamSingle(JsonReader reader)
			throws IOException {
		reader.beginObject();
		try {
			return parseJsonWeather(reader);
		}
		finally {
			reader.endObject();
		}
	}

	/**
	 * Parse a Json stream and convert it into a List of JsonWeather
	 * objects.
	 */
	public List<JsonWeather> parseJsonWeatherArray(JsonReader reader)
			throws IOException {

		List<JsonWeather> list = new ArrayList<JsonWeather>();
		reader.beginObject();
		try {
			if (reader.peek() == JsonToken.END_ARRAY)
				return null;

			list.add(parseJsonStreamSingle(reader));
			return list;
		} finally {
			reader.endObject();
		}

	}

	/**
	 * Parse a Json stream and return a JsonWeather object.
	 */
	public JsonWeather parseJsonWeather(JsonReader reader) 
			throws IOException {

		JsonWeather jsonWeather = new JsonWeather();

		reader.beginObject();

		try {
			while (reader.hasNext()) {
				String name = reader.nextName();
				if (name.equals("cod")) {
					jsonWeather.setCod(reader.nextLong());
				} 
				else if (name.equals("name")) { 
					jsonWeather.setName(reader.nextString());
				}
				else if (name.equals("id")) {
					jsonWeather.setId(reader.nextLong());
				} 
				else if (name.equals("dt")) { 
					jsonWeather.setDt(reader.nextLong());
				}
				else if (name.equals("wind")) { 
					jsonWeather.setWind(parseWind(reader));
				}
				else if (name.equals("main")) { 
					jsonWeather.setMain(parseMain(reader));
				}
				else if (name.equals("base")) { 
					jsonWeather.setBase(reader.nextString());
				}
				else if (name.equals("weather")) { 
					jsonWeather.setWeather(parseWeathers(reader));
				}
				else if (name.equals("sys")) { 
					jsonWeather.setSys(parseSys(reader));
				}
				else {
					reader.skipValue();
				}
			}
			return jsonWeather;
		}
		finally {
			reader.endObject();
		}
	}

	/**
	 * Parse a Json stream and return a List of Weather objects.
	 */
	public List<Weather> parseWeathers(JsonReader reader) throws IOException {
		List<Weather> weathers = new ArrayList<Weather>();
		reader.beginObject();

		try {
			while(reader.hasNext()) {
				String name = reader.nextName();
				if (name.equals("weather")) {
					weathers.add(parseWeather(reader));
				} 
				else
					reader.skipValue();
			}
			return weathers;
		}
		finally {
			reader.endObject();	
		}

	}

	/**
	 * Parse a Json stream and return a Weather object.
	 */
	public Weather parseWeather(JsonReader reader) throws IOException {
		Weather weather = new Weather();

		reader.beginArray();

		try {
			while (reader.hasNext()) {
				String name = reader.nextName();
				if (name.equals("id")) {
					weather.setId(reader.nextLong());
				} 
				else if (name.equals("main")) { 
					weather.setMain(reader.nextString());
				}
				else if (name.equals("description")) {
					weather.setDescription(reader.nextString());
				} 
				else if (name.equals("icon")) { 
					weather.setIcon(reader.nextString());
				}
				else {
					reader.skipValue();
				}
			}
			return weather;
		}
		finally {
			reader.endArray();
		}
	}

	/**
	 * Parse a Json stream and return a Main Object.
	 */
	public Main parseMain(JsonReader reader) 
			throws IOException {
		Main main = new Main();

		reader.beginObject();

		while (reader.hasNext()) {
			String name = reader.nextName();
			if (name.equals("temp")) {
				main.setTemp(reader.nextDouble());
			} 
			else if (name.equals("tempMin")) { 
				main.setTempMin(reader.nextDouble());
			}
			else if (name.equals("tempMax")) {
				main.setTempMax(reader.nextDouble());
			} 
			else if (name.equals("pressure")) { 
				main.setPressure(reader.nextDouble());
			}
			else if (name.equals("seaLevel")) { 
				main.setSeaLevel(reader.nextDouble());
			}
			else if (name.equals("grndLevel")) {
				main.setGrndLevel(reader.nextDouble());
			} 
			else if (name.equals("humidity")) { 
				main.setHumidity(reader.nextLong());
			} 
			else {
				reader.skipValue();
			}
		}

		reader.endObject();
		return main;
	}

	/**
	 * Parse a Json stream and return a Wind Object.
	 */
	public Wind parseWind(JsonReader reader) throws IOException {
		Wind wind = new Wind();

		reader.beginObject();

		while (reader.hasNext()) {
			String name = reader.nextName();
			if (name.equals("deg")) {
				wind.setDeg(reader.nextDouble());
			} 
			else if (name.equals("speed")) { 
				wind.setSpeed(reader.nextDouble());
			} 
			else {
				reader.skipValue();
			}
		}

		reader.endObject();
		return wind;
	}

	/**
	 * Parse a Json stream and return a Sys Object.
	 * @throws IOException 
	 */
	public Sys parseSys(JsonReader reader) throws IOException {
		Sys sys = new Sys();

		reader.beginObject();

		while (reader.hasNext()) {
			String name = reader.nextName();
			if (name.equals("message")) {
				sys.setMessage(reader.nextDouble());
			} 
			else if (name.equals("country")) { 
				sys.setCountry(reader.nextString());
			} 
			else if (name.equals("sunrise")) {
				sys.setSunrise(reader.nextLong());
			} 
			else if (name.equals("sunset")) {
				sys.setSunset(reader.nextLong());
			}
			else {
				reader.skipValue();
			}
		}
		reader.endObject();   
		return sys;
	}
}
