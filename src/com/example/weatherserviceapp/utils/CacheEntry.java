package com.example.weatherserviceapp.utils;

import com.example.weatherserviceapp.aidl.WeatherData;

/**
 * Simple POJO to store an entry of the cache..
 */
public class CacheEntry {

	 // Marks the expire time in milliseconds.
    public long cachedUntil;

    // Weather data entry
    public WeatherData data;
}
