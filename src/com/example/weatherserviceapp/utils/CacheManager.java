package com.example.weatherserviceapp.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.example.weatherserviceapp.aidl.WeatherData;

public class CacheManager {
	private static HashMap<String, CacheEntry> mBackingStore = new HashMap<String, CacheEntry>();
	private static int cacheDurationInSeconds = 30;
	
	/**
	 * Attempts to find an entry in the cache for the given key.
	 */
	public static WeatherData get(String key) {
		updateCache();

		Date date = new Date();
		CacheEntry cached = mBackingStore.get(key.toLowerCase());

		if (cached != null && date.getTime() < cached.cachedUntil) {
			return cached.data;
		}

		return null;
	}

	/**
	 * Adds an Weather data entry to the cache.
	 */
	public static synchronized void set(String key, WeatherData weatherData) {
		Date date = new Date();
		long expire = date.getTime() + (cacheDurationInSeconds * 1000); // in milliseconds

		CacheEntry entry = new CacheEntry();
		entry.data = weatherData;
		entry.cachedUntil = expire;

		mBackingStore.put(key, entry);
	}

	private static synchronized void updateCache() {
		HashMap<String, CacheEntry> map = new HashMap<>();
		Date date = new Date();

		for (Map.Entry<String, CacheEntry> cacheEntrySet : mBackingStore.entrySet()) {
			if (cacheEntrySet.getValue().cachedUntil > date.getTime()) {
				map.put(cacheEntrySet.getKey().toLowerCase(), cacheEntrySet.getValue());
			}
		}

		mBackingStore = map; // update the new list.
	}
}
