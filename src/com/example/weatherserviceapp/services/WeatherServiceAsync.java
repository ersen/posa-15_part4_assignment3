package com.example.weatherserviceapp.services;

import java.util.ArrayList;
import java.util.List;

import com.example.weatherserviceapp.aidl.WeatherData;
import com.example.weatherserviceapp.aidl.WeatherRequest;
import com.example.weatherserviceapp.aidl.WeatherResults;
import com.example.weatherserviceapp.utils.CacheManager;
import com.example.weatherserviceapp.utils.Utils;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

public class WeatherServiceAsync extends LifecycleLoggingService {
	/**
	 * Factory method that makes an Intent used to start the
	 * WeatherServiceAsync when passed to bindService().
	 * 
	 * @param context
	 *            The context of the calling component.
	 */
	public static Intent makeIntent(Context context) {
		return new Intent(context, WeatherServiceAsync.class);
	}

	/**
	 * Called when a client (e.g., WeatherActivity) calls
	 * bindService() with the proper Intent.  Returns the
	 * implementation of WeatherRequest, which is implicitly cast as
	 * an IBinder.
	 */
	@Override
	public IBinder onBind(Intent intent) {
		return mWeatherRequestImpl;
	}

	/**
	 * The concrete implementation of the AIDL Interface
	 * WeatherRequest, which extends the Stub class that implements
	 * WeatherRequest, thereby allowing Android to handle calls across
	 * process boundaries.  This method runs in a separate Thread as
	 * part of the Android Binder framework.
	 * 
	 * This implementation plays the role of Invoker in the Broker
	 * Pattern.
	 */
	WeatherRequest.Stub mWeatherRequestImpl = new WeatherRequest.Stub() {
		/**
		 * Implement the AIDL WeatherRequest getCurrentWeather()
		 * method, which forwards to DownloadUtils getResults() to
		 * obtain the results from the Weather Web service and
		 * then sends the results back to the Activity via a
		 * callback.
		 */
		@Override
		public void getCurrentWeather(String input, WeatherResults callback)
				throws RemoteException {

			List<WeatherData> results = new ArrayList<WeatherData>();

			WeatherData data = CacheManager.get(input);
			if (data != null) {
				Log.i(TAG, "read from cache!");
				results.add(data);
			}
			else {
				Log.i(TAG, "no record found on cache!");
				// Call the Weather Web service to get the current weather
				results = Utils.getResults(input);
			}
			if (results != null) {
				Log.d(TAG, "" 
						+ results.size() 
						+ " results for weather: " 
						+ input);
				callback.sendResults(results);
			} 
		}
	};
}
