package com.example.weatherserviceapp;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import com.example.weatherserviceapp.operations.WeatherOps;
import com.example.weatherserviceapp.operations.WeatherOpsImpl;

public class WeatherActivity extends LifecycleLoggingActivity {
    /**
     * Provides acronym-related operations.
     */
    private WeatherOps weatherOps;

    /**
     * Hook method called when a new instance of Activity is created.
     * One time initialization code goes here, e.g., runtime
     * configuration changes.
     *
     * @param Bundle object that contains saved state information.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Always call super class for necessary
        // initialization/implementation.
        super.onCreate(savedInstanceState);

        // Create the AcronymOps object one time.
        weatherOps = new WeatherOpsImpl(this);

        // Initiate the service binding protocol.
        weatherOps.bindService();
    }

    /**
     * Hook method called by Android when this Activity is
     * destroyed.
     */
    @Override
    protected void onDestroy() {
        // Unbind from the Service.
        weatherOps.unbindService();

        // Always call super class for necessary operations when an
        // Activity is destroyed.
        super.onDestroy();
    }

    /**
     * Hook method invoked when the screen orientation changes.
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        weatherOps.onConfigurationChanged(newConfig);
    }

    /*
     * Initiate the synchronous acronym lookup when the user presses
     * the "Look Up Sync" button.
     */
    public void getCurrentWeatherSync(View v) {
        weatherOps.getCurrentWeatherSync(v);
    }

    /*
     * Initiate the asynchronous acronym lookup when the user presses
     * the "Look Up Async" button.
     */
    public void getCurrentWeatherAsync(View v) {
        weatherOps.getCurrentWeatherAsync(v);
    }
}
